# Motivation

- Project provides LWC to show all the org limits in a single view. Work is underway to add more analytics features and D3 charting capabilities.
- Show how to implement custom types such as embedding progress ring inside lightning data table
- Most importantly show how an **automated CICD pipeline** shouble be implemented for salesforce package based development model
- Demonstrate how to use third party libraries such as D3 charts in LWC - WORK INPROGRESS

![Org Limits Dashboard](OrgLimitsDashboard.png)

## Automated CICD pipeline

![CICD Pipeline](cicd-pipeline.png)

## Features in the package

- "Org Limits Analytics" Lightning app that shows live org limits
- Batch job that can collect org limits on a regular basis
- Historic_Org_Limit_Usage\_\_c that stores historic org limits for analytic purposes
- Custom Reports and Dashboards can be built to analyze various org limits

## How to deploy component ?

- [Installing the app using a Scratch Org](#installing-the-app-using-a-scratch-org): This is the recommended installation option. Use this option if you are a developer who wants to experience the app and the code.

- [Installing the app using an Unlocked Package](#installing-the-app-using-an-unlocked-package): This option allows anybody to experience the sample app without installing a local development environment.

- [Installing the app using a Developer Edition Org or a Trailhead Playground](#installing-the-app-using-a-developer-edition-org-or-a-trailhead-playground): Useful when tackling Trailhead Badges or if you want the app deployed to a more permanent environment than a Scratch org.

- [Optional installation instructions](#optional-installation-instructions)

## Installing the app using a Scratch Org

1. Set up your environment. Follow the steps in the [Quick Start: Lightning Web Components](https://trailhead.salesforce.com/content/learn/projects/quick-start-lightning-web-components/) Trailhead project. The steps include:

   - Enable Dev Hub in your Trailhead Playground
   - Install Salesforce CLI
   - Install Visual Studio Code
   - Install the Visual Studio Code Salesforce extensions, including the Lightning Web Components extension

1. If you haven't already done so, authorize your hub org and provide it with an alias (**myhuborg** in the command below):

   ```
   sfdx auth:web:login -d -a myhuborg
   ```

1. Clone the orglimitsdashboardcicd repository:

   ```
   git clone https://gitlab.com/sritheturbocoder/orglimitsdashboardcicd.git
   cd OrgLimitsDashboardCICD
   ```

1. Create a scratch org and provide it with an alias (**org-limits** in the command below):

   ```
   sfdx force:org:create -s -f config/project-scratch-def.json -a org-limits
   ```

1. Push the app to your scratch org:

   ```
   sfdx force:source:push
   ```

1. Open the scratch org:

   ```
   sfdx force:org:open
   ```

1. In App Launcher, click **View All** then select the **Org Limits Analytics** app.

1. For collecting historical org limits schedule batch job for class SchedulerForOrgLimitscollector

1. Manage records for OrgLimits custom metadata and set value for Days_to_keep_Org_Limit_Metric. Org metrics older than this value will be deleted to save on storage space

## Installing the app using an Unlocked Package

Follow this set of instructions if you want to deploy the app to a more permanent environment than a Scratch org or if you don't want to install the local developement tools. You can use a non source-tracked orgs such as a free [Developer Edition Org](https://developer.salesforce.com/signup) or a [Trailhead Playground](https://trailhead.salesforce.com/).

Make sure to start from a brand-new environment to avoid conflicts with previous work you may have done.

1. Log in to your org

1. Click [this link](https://login.salesforce.com/packaging/installPackage.apexp?p0=04t5g000000cOpXAAU) to install the Org Limits Dashboard unlocked package in your org.

1. Select **Install for Admin Users**

1. In App Launcher, click **View All** then select the **Org Limits Analytics** app.

1. For collecting historical org limits schedule batch job for class SchedulerForOrgLimitscollector

1. Manage records for OrgLimits custom metadata and set value for Days_to_keep_Org_Limit_Metric. Org metrics older than this value will be deleted to save on storage space

## Installing the App using a Developer Edition Org or a Trailhead Playground

Follow this set of instructions if you want to deploy the app to a more permanent environment than a Scratch org.
This includes non source-tracked orgs such as a [free Developer Edition Org](https://developer.salesforce.com/signup) or a [Trailhead Playground](https://trailhead.salesforce.com/).

Make sure to start from a brand-new environment to avoid conflicts with previous work you may have done.

1. Clone this repository:

   ```
   git clone https://gitlab.com/sritheturbocoder/orglimitsdashboardcicd.git
   cd OrgLimitsDashboardCICD
   ```

1. Authorize your Trailhead Playground or Developer org and provide it with an alias (**mydevorg** in the command below):

   ```
   sfdx auth:web:login -s -a mydevorg
   ```

1. Run this command in a terminal to deploy the app.

   ```
   sfdx force:source:deploy -p force-app
   ```

1. If your org isn't already open, open it now:

   ```
   sfdx force:org:open -u mydevorg
   ```

1. In App Launcher, click **View All** then select the **Org Limits Analytics** app.

1. For collecting historical org limits schedule batch job for class SchedulerForOrgLimitscollector

1. Manage records for OrgLimits custom metadata and set value for Days_to_keep_Org_Limit_Metric. Org metrics older than this value will be deleted to save on storage space

## Final words

Work is underway to add more analytics features and D3 charting capabilities.

This development is under progress so use at your own risk :-)
