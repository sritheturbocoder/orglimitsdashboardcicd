import { LightningElement, wire } from "lwc";
import { refreshApex } from "@salesforce/apex";
import listOfOrgLimits from "@salesforce/apex/OrgLimitsController.getOrgLimits";

const COLS = [
  { label: "Limit Name", fieldName: "limitName" },
  { label: "Limit Usage", fieldName: "limitUsage" },
  { label: "Max Limit", fieldName: "maxLimit" },
  { label: "Usage Percentage", fieldName: "usagePercentage" },
  { label: "Usage", fieldName: "usage", type: "proRing" }
];

export default class OrgLimitsDashboard extends LightningElement {
  clickedButtonLabel;
  columns = COLS;
  orglimits;

  @wire(listOfOrgLimits)
  imperativeWiring(result) {
    this.orglimits = result;
  }

  renderedCallback() {
    // eslint-disable-next-line @lwc/lwc/no-async-operation
    window.setTimeout(() => {
      this.handleRefresh();
    }, 1800000); //every 30 minutes
  }

  handleRefresh() {
    return refreshApex(this.orglimits);
  }
}
