public with sharing class SchedulerForOrgLimitscollector implements Schedulable {
  public void execute(SchedulableContext ctx) {
    System.debug('calling hourly limits');
    OrgLimitsDataCollector.collectOrgLimits();
    OrgLimitsDataCollector.cleanUp();
  }
}
