@IsTest
public with sharing class OrgLimitsDataCollectorTests {
  static testMethod void testCountofHistoricOrgLimitGreaterThan0() {
    Test.startTest();
    OrgLimitsDataCollector.collectOrgLimits();
    List<Historic_Org_Limit_Usage__c> historicLimits = [
      SELECT ID
      FROM Historic_Org_Limit_Usage__c
    ];
    System.assert(historicLimits.size() > 0, 'Limits must be greater than 0');
    Test.stopTest();
  }

  static testMethod void testIfCleanUpOfOldMetricsRecordWorks() {
    Test.startTest();
    OrgLimitsDataCollector.collectOrgLimits();
    OrgLimitsDataCollector.cleanUp(); // cleanup is going to delete records older than what's given in custom metadata for now its set as 1
    List<Historic_Org_Limit_Usage__c> historicLimits = [
      SELECT ID
      FROM Historic_Org_Limit_Usage__c
    ];
    System.assert(historicLimits.size() > 0, 'Limits must be greater than 0');
    Test.stopTest();
  }
}
