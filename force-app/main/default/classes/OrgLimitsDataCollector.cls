public with sharing class OrgLimitsDataCollector {
  public static void collectOrgLimits() {
    System.debug('collecting hourly org limits');
    Map<String, System.OrgLimit> limitsMap = OrgLimits.getMap();
    List<Historic_Org_Limit_Usage__c> historicLimits = new List<Historic_Org_Limit_Usage__c>();

    for (String key : limitsMap.keySet()) {
      System.OrgLimit apiRequestsLimit = limitsMap.get(key);
      Historic_Org_Limit_Usage__c historicLimit = new Historic_Org_Limit_Usage__c();
      historicLimit.Limit_Name__c = apiRequestsLimit.getName();
      historicLimit.Limit_Usage__c = apiRequestsLimit.getValue();
      historicLimit.Max_Limit__c = apiRequestsLimit.getLimit();
      if (historicLimit.Max_Limit__c > 0) {
        historicLimit.Usage__c = ((historicLimit.Limit_Usage__c /
          historicLimit.Max_Limit__c) * 100)
          .round()
          .intValue();
      } else {
        historicLimit.Usage__c = 0;
      }

      historicLimit.Usage_Percentage__c = historicLimit.Usage__c + ' %';
      historicLimit.Measurement_Hour__c = System.now();
      historicLimits.add(historicLimit);
    }

    if (Historic_Org_Limit_Usage__c.sObjectType.getDescribe().isCreateable()) {
      insert historicLimits;
    }
  }

  public static void cleanUp() {
    List<OrgLimits__mdt> orgLimitsMetadata = [
      SELECT Number_of_days_to_keep_org_limits_metric__c
      FROM OrgLimits__mdt
      WHERE DEVELOPERNAME = 'Days_to_keep_Org_Limit_Metric'
      WITH SECURITY_ENFORCED
    ];
    Date expiryDate = Date.today()
      .addDays(
        -Integer.valueOf(
          orgLimitsMetadata[0].Number_of_days_to_keep_org_limits_metric__c
        )
      );
    System.debug(
      'orgLimitsMetadata[0].Number_of_days_to_keep_org_limits_metric__c : ' +
      orgLimitsMetadata[0].Number_of_days_to_keep_org_limits_metric__c
    );
    System.debug('Expiry date for deletion is : ' + expiryDate);
    List<Historic_Org_Limit_Usage__c> tobeDeleted = [
      SELECT ID
      FROM Historic_Org_Limit_Usage__c
      WHERE LastModifiedDate < :expiryDate
    ];
    System.debug('tobeDeleted.size() is : ' + tobeDeleted.size());
    if (
      tobeDeleted.size() > 0 &&
      Historic_Org_Limit_Usage__c.sObjectType.getDescribe().isDeletable()
    ) {
      delete tobeDeleted;
      System.debug('deleted records');
    }
  }
}
