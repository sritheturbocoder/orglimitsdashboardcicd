@IsTest
public with sharing class SchedulerTestClass {
  static testMethod void scheduleTest() {
    String hourlyCronExpression = '0 0 * * * ?';
    Test.startTest();
    String jobId = System.schedule(
      'hourly-SchedulerForOrgLimitscollector',
      hourlyCronExpression,
      new SchedulerForOrgLimitscollector()
    );
    CronTrigger ct = [
      SELECT Id, CronExpression, TimesTriggered, NextFireTime
      FROM CronTrigger
      WHERE id = :jobId
    ];
    System.assertEquals(
      hourlyCronExpression,
      ct.CronExpression,
      'Cron expression does not match'
    );
    System.assertEquals(0, ct.TimesTriggered, 'Times trigged does not match');
    Test.stopTest();
  }
}
